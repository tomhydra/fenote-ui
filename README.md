## Fenote Administrator

### installation
`$ yarn install`

`$ bower install`

`$ yarn run server` - starts a local server at port `4455`

`$ yarn run dev` - which will initiate less compiler and live reload

### folder and file naming conventions
- static assets live under `static` folder
- folders inside `app` are self explanatory
- controllers must end with `*.controller.js` (same structure for `services`, `directives` and `filters`)

### coding tips and guidelines
- [Angular Style Guide](https://github.com/johnpapa/angular-styleguide) when writing Angular code
- [JavaScript Style Guide by Airbnb](https://github.com/airbnb/javascript/tree/master/es5) when writing JavaScript code
- [JS Doc's](http://usejsdoc.org) commenting "syntax" when commenting on code
- to avoid unnecessary merge conflicts please use `npm` and `bower` to manage dependencies (properly use `--save` and `--save-dev` when managing dependencies)

