/* eslint-disable */ ;
(function(angular) {
  'use strict';

  angular
    .module('app')
    .config(Config);

  Config.$inject = ['$compileProvider', '$stateProvider', '$locationProvider'];

  function Config($compileProvider, $stateProvider, $locationProvider) {
    $compileProvider.debugInfoEnabled(false);

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/view/home.html',
        controller: 'HomeController as home',
      })
      .state('login', {
        url: '/login',
        templateUrl: 'app/view/login.html',
      })
      .state('sign-up', {
        url: '/sign-up',
        templateUrl: 'app/view/sign-up.html',
      })
      .state('gsm', {
        url: '/gsm',
        templateUrl: 'app/view/gsm.html',
        controller: 'GSMController as gsm',
      })
      .state('editor', {
        url: '/edit',
        templateUrl: 'app/view/editor.html',
      })
      .state('editor.bus', {
        url: '/bus',
        templateUrl: 'app/view/edit/bus.html',
        controller: 'BusController as editor',
      })
      .state('editor.route', {
        url: '/route',
        templateUrl: 'app/view/edit/route.html',
        controller: 'RouteController as editor',
      })
      .state('editor.log', {
        url: '/log',
        templateUrl: 'app/view/edit/log.html',
        controller: 'LogController as editor',
      })
      .state('editor.station', {
        url: '/station',
        templateUrl: 'app/view/edit/station.html',
        controller: 'StationController as editor',
      })
      .state('editor.station-route', {
        url: '/station-route',
        templateUrl: 'app/view/edit/station-route.html',
        controller: 'EditController as editor',
      });

  }
})(window.angular);
