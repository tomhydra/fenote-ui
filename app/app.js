/* eslint-disable */
/* my eslint is configured for React and ES6 --- remove line to see the rules on your machine */
;(function(angular) {
  angular.module('app', ['ngAnimate',
                         'ngSanitize',
                         'ngMessages',
                         'ui.router',
                         'ui.bootstrap',
                         'restangular',
                         'dndLists',
                         'directive.divider',
                         'directive.no-record',
                         'highcharts-ng',
                         'service.rest',
                        ]);
})(window.angular);
