/* eslint-disable */
;(function(angular) {
  'use strict';

  angular
    .module('app')
    .run(Run);

  Run.$inject = ['$rootScope'];

  function Run($rootScope) {
    console.log('Running');
    $rootScope.appRouteState = 'home';
    $rootScope.apiHost = 'http://127.0.0.1:8000/';
    $rootScope.isLoggedIn = false;

    // this is used for activating navigation items
    $rootScope.$on('$stateChangeSuccess', function (event, toState, toParams, fromState, fromParams) {
      if(toState.redirect){
        event.preventDefault();
        $rootScope.appRouteState = toState.redirect;
        $state.go(toState.redirect);
      }else{
        $rootScope.appRouteState = toState.name;
      }
      $("body").animate({ scrollTop: 0 }, 350);
    });

  }
})(window.angular);
