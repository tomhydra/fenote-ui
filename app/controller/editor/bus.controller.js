
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('BusController', BusController);

  BusController.$inject = ["$scope", "$state", "EditorService"];
  function BusController($scope, $state, EditorService) {
    var vm = this;

    vm.routes = [];
    vm.buses =[];
    vm.loading = false;
    vm.form = {
      'code' : '',
      'route_id': '',
    };

    vm.onSubmit = onSubmit;
    vm.deleteBus = deleteBus;

    init();

    function init() {
      vm.loading = true;
      EditorService.allRoutes()
        .then(function (result) {
          vm.loading = false;
          vm.routes = result.data;
        });

      EditorService.allBuses()
        .then(function (result) {
          vm.buses = result.data;
        });

      vm.form = {
        'code' : '',
        'route_id': '',
      };
    }

    function onSubmit(e) {
      e.preventDefault();
      vm.loading = true;
      EditorService.saveBus(vm.form)
        .then(function (result) {
          notie.alert(1, "Bus saved successfully!", 2);
          vm.loading = false;
          init();
        })
        .catch(function (error) {
          notie.alert(3, error.data.error.errors['code'], 2);
          vm.loading = false;
        })
    }

    function deleteBus(id) {
      notie.confirm('Are you sure you want to delete this bus?', 'Yes', 'No', function () {
        EditorService.deleteBus(id)
          .then(function (result) {
            init();
          });
      });
    }



  }

})(window.angular);

