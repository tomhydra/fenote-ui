
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('LogController', LogController);

  LogController.$inject = ["$scope", "$state", "EditorService"];
  function LogController($scope, $state, EditorService) {
    var vm = this;

    vm.deleteLog = deleteLog;
    vm.logs = [];
    vm.loading = false;

    init();

    function init() {
      vm.loading = true;
      EditorService.allLogs()
        .then(function (restult) {
          vm.loading = false;
          vm.logs = restult.data;
        });
    }

    function deleteLog(id) {
      notie.confirm('Are you sure you want to delete this item?', 'Yes', 'No', function () {
        EditorService.deleteLog(id)
          .then(function (item) {
            init();
          })
      });

    }
  }

})(window.angular);

