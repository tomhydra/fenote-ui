
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('RouteController', RouteController);

  RouteController.$inject = ["$scope", "$state", "EditorService"];
  function RouteController($scope, $state, EditorService) {
    var vm = this;

    vm.saveRoute = saveRoute;
    vm.deleteRoute = deleteRoute;

    vm.station = '';
    vm.stationMulti = [];
    vm.stationList = [];
    vm.routes = [];
    vm.loading = false;

    vm.stations = {
      selected: null,
      data: [],
    };

    vm.form = {
      name: '',
      stations: [],
    };

    init();

    function init() {
      EditorService.allStations()
        .then(function (result) {
          vm.stationList = result.data;
        });
      getRoutes();
    }

    function getRoutes() {
      vm.loading = true;
      EditorService.allRoutes()
        .then(function (result) {
          vm.loading = false;
          vm.routes = result.data;
        })
    }

    function deleteRoute(id) {
      notie.confirm('Are you sure you want to delete this route?', 'Yes', 'No', function () {
        EditorService.deleteRoute(id)
          .then(function (result) {
            getRoutes();
          });
      });
    }

    function saveRoute(e) {
      e.preventDefault();
      vm.loading = true;
      if (!vm.stations.data.length || vm.stations.data.length < 2){
        notie.alert(3, "You must add at least 2 station", 2);
        vm.loading = false;
        return;
      }
      vm.form.stations = vm.stations.data.map(function (item) {
        return item.id;
      });
      EditorService.saveRoute(vm.form)
        .then(function (result) {
          notie.alert(1, "Route saved successfully!", 2);
          vm.loading = false;
          $state.reload();
        }, function (error) {
          vm.loading = false;
          notie.alert(3, error.data.error.errors['name'], 2);
        });

    }

    $scope.$watch(function () {
      return vm.stationMulti;
    }, function (newVal, oldVal) {
      vm.stations.data = angular.copy(newVal);

    });

    // Jquery code should be used within a directive!! Don't use this!
    $('document').ready(function () {
      $('#my-select').multiSelect();
    });


  }

})(window.angular);

