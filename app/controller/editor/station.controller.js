
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('StationController', StationController);

  StationController.$inject = ["$scope", "$state", "EditorService"];
  function StationController($scope, $state, EditorService) {
    var vm = this;

    vm.stations =[];
    vm.loading = false;
    vm.form = {
      'name' : '',
    };

    vm.onSubmit = onSubmit;
    vm.deleteStation = deleteStation;

    init();

    function init() {
      vm.loading = true;
      EditorService.allStations()
        .then(function (result) {
          vm.loading = false;
          vm.stations = result.data;
        });

      vm.form = {
        'name' : '',
      };

    }

    function onSubmit(e) {
      e.preventDefault();
      vm.loading = true;
      EditorService.saveStation(vm.form)
        .then(function (result) {
          notie.alert(1, "Station saved successfully!", 2);
          vm.loading = false;
          init();
        })
        .catch(function (error) {
          vm.loading = false;
          notie.alert(3, error.data.error.errors['name'], 2);
        })
    }

    function deleteStation(id) {
      notie.confirm('Are you sure you want to delete this station?', 'Yes', 'No', function () {
        EditorService.deleteStation(id)
          .then(function (result) {
            init();
          });
      });
    }



  }

})(window.angular);

