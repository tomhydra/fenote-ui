
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('GSMController', GSMController);

  GSMController.$inject = ["$scope", "$state", "EditorService"];
  function GSMController($scope, $state, EditorService) {
    var vm = this;

    vm.stations = [];
    vm.buses = [];
    vm.busesCopy = [];


    vm.form = {
      'ss': '',
      'cd': '',
      'ps': '',
      'ct': '',
      'at': '',
      'st': 'active',
    };

    vm.reset = function () {
      vm.form = {
        'ss': '',
        'cd': '',
        'ps': '',
        'ct': '',
        'at': '',
        'st': 'active',
      };
    };

    vm.statuses = [
      {
        value: 'active',
        name: 'Active'
      },{
        value: 'accident',
        name: 'Accident'
      },{
        value: 'break',
        name: 'Break'
      },{
        value: 'stop',
        name: 'Stop'
      },
    ];
    vm.onSubmit = onSubmit;
    vm.filterBuses = filterBuses;

    getStations();
    getBuses();

    function filterBuses(station) {
      vm.buses = vm.busesCopy.filter(function (item) {
        var found = false;
        item.route.data.stations.data.forEach(function (st) {
          if (st.name === station){
            found = true;
          }
        });
        return found;
      })
    }

    function getStations() {
      EditorService.allStations()
        .then(function (result) {
          vm.stations = result.data;
        })
    }

    function getBuses() {
      EditorService.allBuses()
        .then(function (result) {
          vm.buses = result.data;
          vm.busesCopy = angular.copy(vm.buses);
        })
    }

    function onSubmit(e) {
      e.preventDefault();
      EditorService.saveLog(vm.form)
        .then(function (result) {
          notie.alert(1, 'Saved successfully!', 2);
          vm.reset();
        })
        .catch(function (error) {
          notie.alert(3, "Error saving", 2);
        });
    }

  }

})(window.angular);

