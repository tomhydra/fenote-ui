
(function (angular) {
  "use strict";

  angular
    .module('app')
    .controller('HomeController', HomeController);

  HomeController.$inject = ["$scope", "$state", "EditorService", "$interval", "$uibModal"];
  function HomeController($scope, $state, EditorService, $interval, $uibModal) {
    var vm = this;
    $scope.isLoggedIn = true;
    console.log("home controller");
    vm.showDetail = showDetail;
    var modalInstance = null;

    vm.labels = {
      buses : '',
      stations: '',
      damages: '',
      routes: '',
      passengerChart: '',
      busChart: ''
    };

    vm.passengersBarChartConfig = {
      chart: {
        type: 'column'
      },
      title: {
        text: 'Daily passenger status'
      },
      subtitle: {
        text: ''
      },
      xAxis: {
        categories: [
          '11-5-2017',
          '12-5-2017',
          '13-5-2017',
          '14-5-2017',
          '15-5-2017',
          '16-5-2017',
          '17-5-2017',
          '18-5-2017',
          '19-5-2017',
          '20-5-2017',
        ],
        crosshair: true
      },
      yAxis: {
        min: 0,
        title: {
          text: 'Passengers'
        }
      },
      tooltip: {
        headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
        pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
        '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
        footerFormat: '</table>',
        shared: true,
        useHTML: true
      },
      plotOptions: {
        column: {
          pointPadding: 0.2,
          borderWidth: 0
        }
      },
      series: [{
        name: '4 Kilo to 6 Kilo',
        data: [49, 71, 106, 129, 144, 176, 135, 148, 216, 194]

      }, {
        name: '6 Kilo to Shiro Meda',
        data: [83, 78, 98, 93, 106, 84, 105, 104, 91, 83]

      }, {
        name: 'Piassa to Bole',
        data: [48, 38, 39, 41, 47, 48, 59, 59, 52, 65]

      }, {
        name: '4 Kilo to Mexico',
        data: [42, 33, 34, 39, 52, 75, 57, 60, 47, 39]

      }]
    }

    vm.busPieChartConfig = {
      chart: {
        plotBackgroundColor: null,
        plotBorderWidth: null,
        plotShadow: false,
        type: 'pie'
      },
      title: {
        text: 'Buses by number of Passengers'
      },
      tooltip: {
        pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
      },
      plotOptions: {
        pie: {
          allowPointSelect: true,
          cursor: 'pointer',
          dataLabels: {
            enabled: false
          },
          showInLegend: true
        }
      },
      series: [{
        name: 'Buses',
        colorByPoint: true,
        data: vm.labels.busChart,
      }]
    }
    vm.liveData = [];

    var dataCount = -1;
    vm.loading = true;
    liveUpdate();
    getStats();
    $interval(function () {
      liveUpdate();
    }, 500);

    function liveUpdate() {
      EditorService.allBusesWithLogs()
        .then(function (result) {
          vm.loading = false;
          if (dataCount < 0){
            updateData(result.data);
          }
          var newCount = result.data.length;
          if (dataCount !== newCount){
            dataCount = newCount;
            updateData(result.data);
          }
        })
    }

    function getStats() {
      EditorService.getStats()
        .then(function (result) {
          vm.labels = result;
          vm.busPieChartConfig.series[0].data = vm.labels.busChart;
        });
    }

    function updateData(liveData) {
      vm.liveData = liveData.filter(function (data) {
        data.arrival_location = "Unkown";
        data.arrival_time = "Unknown";
        data.logs.data.forEach(function (log) {
          if (log.status === 'coming' && log.time) {
            data.arrival_location = log.station;
            data.arrival_time = log.time;
          }

          if (log.current_log){
            data.current_log = log.station;
            data.status = log.status;
          }
        });
        return data.current_log;
      });
    }

    function showDetail(data) {
      vm.currentLog = data;
      modalInstance = $uibModal.open({
          templateUrl: "stationModal.html",
          scope: $scope,
        });
    }



  }

})(window.angular);

