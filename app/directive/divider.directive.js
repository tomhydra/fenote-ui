
;(function (angular) {
  'use strict';

  angular.module('directive.divider', []);

  angular
    .module('directive.divider')
    .directive('divider', divider);

  divider.$inject = [];

  /**
   * a line divider to be used to divide section on UI
   *
   * @return {Object}
   */
  function divider() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
        margin: '@',
      },
      template: '<div class="row"><div class="col-lg-12"><div class="form-group"><div style="border-top: 1px solid #bdc3c7;border-bottom: 1px solid #ecf0f1;" ng-style="{ margin: margin || \'16px 0\' }"></div></div></div></div>',

    };
  }
})(window.angular);
