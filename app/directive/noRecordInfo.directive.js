
;(function (angular) {
  'use strict';

  angular.module('directive.no-record', []);

  angular
    .module('directive.no-record')
    .directive('noRecord', noRecord);

  noRecord.$inject = [];

  function noRecord() {
    return {
      restrict: 'E',
      replace: true,
      scope: {
      },
      template: '<div class="alert alert-info"> <span class="ti-info-alt"></span> No record! </div>',

    };
  }
})(window.angular);
