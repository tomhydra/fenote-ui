
(function(angular) {
  'use strict';

  angular
    .module('app')
    .service("EditorService", EditorService);

  EditorService.$inject = ['api'];

  function EditorService(api) {
    var service = {
      allStations : allStations,
      allRoutes : allRoutes,
      allBuses : allBuses,
      allBusesWithLogs: allBusesWithLogs,
      allLogs : allLogs,
      getStation : getStation,
      getRoute : getRoute,
      getBus : getBus,
      getLog : getLog,
      getStats : getStats,
      saveStation : saveStation,
      saveRoute : saveRoute,
      saveBus : saveBus,
      saveLog : saveLog,
      updateStation : updateStation,
      updateRoute : updateRoute,
      updateBus : updateBus,
      updateLog : updateLog,
      deleteStation : deleteStation,
      deleteRoute : deleteRoute,
      deleteBus : deleteBus,
      deleteLog : deleteLog,
    };

    return service;

    function allStations() {
      return api.all('stations').get('');
    }

    function allRoutes() {
      return api.all('routes').get('');
    }

    function allBuses() {
      return api.all('buses?include=route').get('');
    }

    function allBusesWithLogs() {
      return api.all('buses?include=logs').get('');
    }

    function allLogs() {
      return api.all('logs').get('');
    }

    function getStation(id) {
      return api.one('stations', id).get('');
    }

    function getRoute(id) {
      return api.one('routes', id).get('');
    }

    function getBus(id) {
      return api.one('buses', id).get('');
    }

    function getLog(id) {
      return api.one('logs', id).get('');
    }

    function getStats() {
      return api.all('stat').get('');
    }

    function saveStation(data) {
      return api.all('stations').customPOST(data);
    }

    function saveRoute(data) {
      return api.all('routes').customPOST(data);
    }

    function saveBus(data) {
      return api.all('buses').customPOST(data);
    }

    function saveLog(data) {
      return api.all('logs').customPOST(data);
    }

    function updateStation(data) {
      return api.all('stations').customPUT(data);
    }

    function updateRoute(data) {
      return api.all('routes').customPUT(data);
    }

    function updateBus(data) {
      return api.all('buses').customPUT(data);
    }

    function updateLog(data) {
      return api.all('logs').customPUT(data);
    }

    function deleteStation(id) {
      return api.one('stations', id).customDELETE('');
    }

    function deleteRoute(id) {
      return api.one('routes', id).customDELETE('');
    }

    function deleteBus(id) {
      return api.one('buses', id).customDELETE('');
    }

    function deleteLog(id) {
      return api.one('logs', id).customDELETE('');
    }


  }

})(window.angular);