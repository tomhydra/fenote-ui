
/* eslint-disable */ ;
(function(angular) {
  'use strict';


  angular.module('service.rest', []);

  angular
    .module('service.rest')
    .constant("appConstant", {
      "api": "http://localhost:8000/api"
    })
    .factory("api", REST);

  /*@ngNoInject*/
  function REST(Restangular, appConstant) {
    /*@ngNoInject*/
    return Restangular.withConfig(function (RestangularConfigurer) {
      RestangularConfigurer.setBaseUrl(appConstant.api);
    });

  }

})(window.angular);