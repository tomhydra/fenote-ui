const gulp = require('gulp');
const concat = require('gulp-concat');
const livereload = require('gulp-livereload');
const less = require('gulp-less');
const autoprefixer = require('gulp-autoprefixer');
const path = require('path');

const watchPath = {
  css: ['app/**/*.css'],
  less: ['app/**/*.less'],
  js: ['app/**/*.js'],
  html: ['index.html', 'app/**/*.html'],
};

// less compiler
gulp.task('less', () => {
  gulp.src(watchPath.less)
      .pipe(less({
        paths: [path.join(__dirname, 'less', 'includes')],
      }))
      .pipe(autoprefixer({
        browsers: ['> 0%'],
        cascade: true,
      }))
      .pipe(concat('style.css'))
      .pipe(gulp.dest('./build/'));
});

// livereload
gulp.task('watch', () => {
  gulp.src([].concat(watchPath.html, watchPath.css, watchPath.less, watchPath.js))
      .pipe(livereload());
});

gulp.task('default', [], () => {
  livereload.listen();
  gulp.watch([].concat(watchPath.html, watchPath.css, watchPath.less, watchPath.js), ['less', 'watch']);
});
